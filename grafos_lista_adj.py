'''
grafos_lista_adj.py
Construção e manutenção de grafos com representação explícita
Group (4):
    - 
    -
    -
    -

Costa, C. J. <cjcosta@utad.pt>
'''
def init_graph(edges, directed=False):
    pass # criação e devolução de um grafo baseado numa lista de edges a implementar pelos alunos

def get_vertices(graph):
    pass # obtenção da lista de vértices de um graph a implementar pelos alunos

def delete_edge(graph, u, v):
    pass # eliminação de um edge de u para v a implementar pelos alunos

def as_edge(graph, u, v):
    pass # testa a existência de uma edge de u para v 


if __name__ == '__main__': # to test module
    # Cria a lista de arestas.
    arestas = [('A', 'B'), ('A', 'C'), ('A', 'D'), ('B', 'E'), ('C', 'B'), ('D', 'E'), ('D', 'F')]

    grafo = init_graph(arestas)
    grafo_resultante = {
                        'A':{'B', 'C','D'},
                        'B':{'A', 'C','E'},
                        'C':{'A', 'B'},
                        'D':{'A', 'E','F'},
                        'E':{'B','D'},
                        'F':{'D'}
                    }
    print('Grafo:', grafo)
    print('Grafo corretamente criado:', grafo == grafo_resultante)
    print('Vértices:', get_vertices(grafo))
    print("Eliminação de ('F', 'D'):", delete_edge(grafo, 'F', 'D'))
    print("Eliminação de ('F', 'E'):", delete_edge(grafo, 'F', 'E'))
    print('Grafo:', grafo)
    print('Vértices:', get_vertices(grafo))
    print("Existe a aresta ('C', 'D'):",as_edge(grafo, 'C', 'F'))
    print("Existe a aresta ('D', 'A'):",as_edge(grafo, 'D', 'A'))
    print("Eliminação de ('D', 'F'):", delete_edge(grafo, 'D', 'F'))
    print('Grafo:', grafo)
    print('Vértices:', get_vertices(grafo))
